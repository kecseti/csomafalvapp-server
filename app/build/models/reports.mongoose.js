const ReportsModel = require('./reports.shema')

async function readById(ObjectId) { 
    if(ObjectId !== undefined){
      const result = await ReportsModel.findOne({_id: ObjectId}); 
      if (!result) {
        console.log(`REPORT DB - Cannot  read from  db an object ${ObjectId}  `)
        return null;
      } else {
        return result;  
      }   
    }
   else return null
}

async function deleteById(ObjectId) { 
  if(ObjectId !== undefined){
    const result = await ReportsModel.remove({_id: ObjectId}); 
    if (!result) {
      console.log(`REPORT DB -  Cannot  read from  db an object ${ObjectId}  `)
      return null;
    } else {
      return result;  
    }   
  }
 else return null
}

async function readAllBetween(lower, upper) { 
  const result = await ReportsModel.find().sort({toSort:-1}).limit(upper-lower).skip(lower).exec()
    if (!result) {
      console.log(`REPORT DB -  Cannot  read from  db objects between ${lower} and ${upper}`)
      return null;
    } else {
      console.log(`REPORT DB -  Just read from the db objects`)
      return result;  
    }   
}

async function create(title, description, photoURL) { 
    const newReportsModel =  new ReportsModel({ title, description, photoURL }); 
    const resultedDoc = await newReportsModel.save()
    console.log(`REPORT DB - Just created in the  db an object ${resultedDoc}`)
    return resultedDoc; 
}

async function count(){
  const result = await ReportsModel.countDocuments({});
  return result;
}

module.exports = { create, readById, readAllBetween, deleteById, count }
