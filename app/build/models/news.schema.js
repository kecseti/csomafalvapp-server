const mongoose  = require('mongoose'); 
const Schema = mongoose.Schema; 

const connectionString=''

mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true  } ).catch ((error) => { console.log(`Ups, cannot connect to database because reasons: ${error}`); }); 
mongoose.connection.on('error', (error)  => { console.log(`Ups, something went wrong during operation with db because reasons: ${error}`); });

mongoose.set('useFindAndModify', false);

const newsSchema = new Schema ({ 
                            title: String,
                            description : String,
                            type : Number,
                            photoURL: String,
                            url: String,
                            toSort: { type: Date, default: Date.now}
                        }); 

module.exports = mongoose.model('news', newsSchema);
