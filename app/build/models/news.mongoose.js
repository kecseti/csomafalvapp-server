const NewsModel = require('./news.schema')

async function readById(ObjectId) { 
    if(ObjectId !== undefined){
      const result = await NewsModel.findOne({_id: ObjectId}); 
      if (!result) {
        console.log(`NEWS DB - Cannot  read from  db an object ${ObjectId}  `)
        return null;
      } else {
        return result;  
      }   
    }
   else return null
}

async function readAllBetween(lower, upper, category) {
  let categories = [1,2,3,4];
  switch(category){
    case 1:{
      categories = [1]
      break
    }
    case 2:{
      categories = [2]
      break
    }
    case 3:{
      categories = [1,2]
      break
    }
    case 4:{
      categories = [3]
      break
    }
    case 5:{
      categories = [1,3]
      break
    }
    case 6:{
      categories = [2,3]
      break
    }
    case 7:{
      categories = [1,2,3]
      break
    }
    case 8:{
      categories = [4]
      break
    }
    case 9:{
      categories = [1,4]
      break
    }
    case 10:{
      categories = [2,4]
      break
    }
    case 11:{
      categories = [1,2,4]
      break
    }
    case 12:{
      categories = [3,4]
      break
    }
    case 13:{
      categories = [1,3,4]
      break
    }
    case 14:{
      categories = [2,3,4]
      break
    }
    case 15:{
      categories = [1,2,3,4]
      break
    }
  }
  const result = await NewsModel.find({'type': { $in: categories }}).sort({toSort:-1}).limit(upper-lower).skip(lower).exec()
    if (!result) {
      console.log(`NEWS DB - Cannot  read from  db objects between ${lower} and ${upper}`)
      return null;
    } else {
      console.log(`NEWS DB - Just read from the db objects`)
      return result;  
    }   
}

async function create(title,description ,type ,photoURL, url) { 
    const newNewsModel =  new NewsModel({ title, description ,type ,photoURL, url}); 
    const resultedDoc = await newNewsModel.save()
    console.log(`NEWS DB - Just created in the  db an object ${resultedDoc}`)
    return resultedDoc; 
}

async function count(){
  const result = await NewsModel.countDocuments({});
  return result;
}

module.exports = { create, readById, readAllBetween, count }

