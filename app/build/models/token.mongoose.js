const TokenModel = require('./token.shema')

async function readAll() { 
  const result = await TokenModel.find().sort({toSort:-1}).exec()
    if (!result) {
      console.log(`TOKEN DB - Cannot  read from  db push tokens`)
      return null;
    } else {
      console.log(`TOKEN DB - Push tokens read`)
      return result;  
    }   
}

async function create(tok) { 
    console.log(`TOKEN DB - Persisting token...`);
    const dbo = await TokenModel.find().sort({toSort:-1}).exec();
    let doc = '';
    if(dbo.length > 0) {
      const arr = new Array(dbo);
      console.log('TOKEN DB - Updating token array...');
      const newTokens = arr[0][0].tokens
      if(!newTokens.includes(tok)) {
        newTokens.push(tok);
      }
      doc = await TokenModel.findOneAndUpdate({_id: arr[0][0]._id}, {tokens: newTokens }, { returnOriginal: false });
      console.log(`TOKEN DB - Just created in the a token object ${doc}`);
    } else {
      console.log(`TOKEN DB - Creating new token...`);
      const newTokenModel =  new TokenModel({ tokens: new Array(tok) }); 
      doc = await newTokenModel.save();
      console.log(`TOKEN DB - Just created in the db a token object ${doc}`);
    }
    return doc; 
}

module.exports = { create, readAll }
