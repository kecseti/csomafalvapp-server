const express = require('express');
const bodyParser = require('body-parser');
const newsMongoDB = require ('../models/news.mongoose');
const tokenMongoDB = require('../models/token.mongoose');
const Expo = require('expo-server-sdk');

const app = express.Router();
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true }));

let expoClient = new Expo.Expo();

app.get('/:lower/:upper?/:categories?',async (request, response) => {
  const { lower, upper, categories} = request.params;
  console.log(`NEWS REST ENDPOINT - GET request for news with params: lower: ${lower}, upper: ${upper}, categories: ${categories}`)
  if(lower && upper){
    newsMongoDB.readAllBetween(Number(lower),Number(upper), Number(categories))
      .then(async(news) => { 
        const count = await newsMongoDB.count();
        console.log(`NEWS REST ENDPOINT - GET Found in the DB total of ${count} news and retriving results`);
        response.status(200).json({status:'SUCCESS: Found',count, news}); 
      })
      .catch((_) =>{ 
        console.log(`NEWS REST ENDPOINT - GET Error reading from the DB!`);
        response.status(404).json({status:'FAILED: Not found or index out of bound'}); 
      }) 
  } else if (lower && !upper && !categories){
      newsMongoDB.readById(lower)
        .then((found) => { response.status(200).json({status:'SUCCESS: Found', news: found});})
        .catch((_) =>{ response.status(404).json({status:'FAILED: Not found'}); })
  } else {
      response.status(422).json({status:'FAILED: No parameter given, please provide the id or a top-bottom index'});
  }  
})

app.post('/', async (request, response) => {
  console.log(`NEWS REST ENDPOINT - POST request received `);
  if(!request.body.title || !request.body.description || !request.body.type || !request.body.photoURL){
    console.log(`NEWS REST ENDPOINT - POST Missing params, cannot proceed `);
    response.status(422).json({status:'FAILED: Bad request'})
  }
  console.log(`NEWS REST ENDPOINT - POST Start persisting `);
  newsMongoDB.create(request.body.title, request.body.description, request.body.type, request.body.photoURL, request.body.url)
  .then(async (created) => { 
    console.log(`NEWS REST ENDPOINT - POST object persisted: ${created}`);
    // Broadcast push notification
    const dbo = await tokenMongoDB.readAll()
    console.log(`NEWS REST ENDPOINT - POST tokens succesfully retrieved from db.`);
    let messages = [];
    for(let token of dbo[0].tokens){
      if (!Expo.Expo.isExpoPushToken(token)) {
        console.error(`NEWS REST ENDPOINT - POST Push token ${token} is not a valid Expo push token`);
        continue;
      }

      messages.push({
        to: token,
        title: created.title,
        body: created.description,
      })
    }
    let chunks = await expoClient.chunkPushNotifications(messages);

    let tickets = [];
    for (let chunk of chunks) {
      try {
        let ticketChunk = await expoClient.sendPushNotificationsAsync(chunk);
        console.log(`NEWS REST ENDPOINT - POST sending push notifiaction: ${ticketChunk}`);
        tickets.push(...ticketChunk);
      } catch (error) {
        console.log(`NEWS REST ENDPOINT - POST error while sending push notifiaction: ${error}`);
      }
    }

    console.log(`NEWS REST ENDPOINT - POST All a-okay! Yey, celebration, fireworks and all!`);
    response.status(201).json({status:'SUCCESS: Persisted', id: created._id}); 
  })
  .catch((_) =>{ 
    console.log(`NEWS REST ENDPOINT - POST Something went wrong, retur code 500!`);
    response.status(500).json({status:'FAILED: Cannot persist object'}); 
  })

})

module.exports = app;
