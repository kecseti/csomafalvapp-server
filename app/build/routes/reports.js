const express = require('express');
const bodyParser = require('body-parser');
const reportsMongoDB = require ('../models/reports.mongoose');

const app = express.Router();
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true }));

app.get('/:lower/:upper?',async (request, response) => {
  const { lower, upper} = request.params;
  console.log(`REPORTS REST ENDPOINT - GET request received with params: lower: ${lower} upper:${upper}`);
  if(lower && upper){
    reportsMongoDB.readAllBetween(Number(lower),Number(upper))
      .then(async(report) => { 
        const count = await reportsMongoDB.count(); 
        console.log(`REPORTS REST ENDPOINT - GET Found in the DB total of ${count} reports and retriving results`);
        response.status(200).json({status:'SUCCESS: Found', count, report}); 
      })
      .catch((_) =>{ 
        console.log(`REPORTS REST ENDPOINT - GET Error reading from the DB!`);
        response.status(404).json({status:'FAILED: Not found or index out of bound'}); 
      }) 
  } else if (lower && !upper){
    reportsMongoDB.readById(lower)
        .then((found) => { response.status(200).json({status:'SUCCESS: Found', report: found});})
        .catch((_) =>{ response.status(404).json({status:'FAILED: Not found'}); })
  } else {
      response.status(422).json({status:'FAILED: No parameter given, please provide the id or a top-bottom index'});
  }  
})

app.post('/', async (request, response) => {
  console.log(`REPORTS REST ENDPOINT - POST request received!`);
  if(!request.body.title || !request.body.description ){
    console.log(`REPORTS REST ENDPOINT - POST title and description not found on the body!`);
    response.status(422).json({status:'FAILED: Bad request'})
  }

  reportsMongoDB.create(request.body.title, request.body.description, request.body.photoURL)
  .then((created) => { 
    console.log(`REPORTS REST ENDPOINT - POST Report persisted succesfully ${created}!`);
    response.status(201).json({status:'SUCCESS: Persisted', id: created._id}); 
  })
  .catch((_) =>{ 
    console.log(`REPORTS REST ENDPOINT - POST something went wrong while persisting, return code 500!`);
    response.status(500).json({status:'FAILED: Cannot persist object'}); 
  })
})

app.delete('/:id', async (request, response) => {
  const { id } = request.params;
  console.log(`${id}`)
  if(id){
    reportsMongoDB.deleteById(id)
      .then((report) => { response.status(200).json({status:'SUCCESS: deleted', report}); })
      .catch((_) =>{ response.status(404).json({status:'FAILED: Not found or index out of bound'}); }) 
  } else {
      response.status(422).json({status:'FAILED: No parameter given, please provide the id to be deleted'});
  }  
})

module.exports = app;
