const express = require('express');
const bodyParser = require('body-parser');
const tokenMongo = require ('../models/token.mongoose');

const app = express.Router();
app.use(express.urlencoded({ extended: true }));

app.post('/', async (request, response) => {
  console.log(`TOKENS REST ENDPOINT - POST request received!`);
  if(!request.body.token){
    console.log(`TOKENS REST ENDPOINT - POST missing token from body!`);
    response.status(422).json({status:'FAILED: Bad request'})
  } else{   
    console.log(`TOKENS REST ENDPOINT - POST token received from body ${request.body.token} !`);
    tokenMongo.create(request.body.token)
    .then((doc) => { 
      console.log(`TOKENS REST ENDPOINT - POST token succesfully persisted!`);
      response.status(201).json({status:'SUCCESS: Persisted token'});
    })
    .catch((err) =>{ 
      console.log(`TOKENS REST ENDPOINT - POST yomething went wrong while persisting the token, return code 500!`);
      response.status(500).json({status:'FAILED: Cannot persist token'});
    })
  } 
})

module.exports = app;
