const express = require('express');
const cors = require('cors')
 
const app = express();

const news = require('./routes/news.js');
const reports = require('./routes/reports.js');
const tokens = require('./routes/tokens.js');

app.set('PORT', process.env.PORT || 5000);
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use('/api/news', news);
app.use('/api/ads', reports);
app.use('/api/tokens', tokens)

app.listen(app.get('PORT'), () => console.log(`Server running on port ${app.get('PORT')}`));

module.exports = app;
